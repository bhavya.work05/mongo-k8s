mongo-k8s
Overview
 I have created deployment for  mongo and mongo-express . demonstarting everything config files

Components Deployed

MongoDB Deployment
Deployment YAML: mongo-deployment.yaml
Service YAML: mongo-service.yaml
ConfigMap YAML: mongodb-configmap.yaml

Mongo Express Deployment
Deployment YAML: mongo-express.yaml
ConfigMap YAML: mongodb-configmap.yaml

Deployment Steps :
MongoDB Deployment

Applied mongo-deployment.yaml to create MongoDB deployment.
Applied mongo-service.yaml to create MongoDB service.

Mongo Express Deployment

Applied mongo-express.yaml to create Mongo Express deployment.
Applied mongodb-configmap.yaml to provide configuration to Mongo Express.
Configuration

Ensure mongodb-secret Secret is created with appropriate keys (mongo-root-username and mongo-root-password).
Ensure mongodb-configmap ConfigMap is created with the correct database_url pointing to mongo-service.
Verification
Verify the deployment by checking
code:
kubectl get all
kubectl get deployment
